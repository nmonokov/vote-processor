package org.devchallenge.vote.processor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Deputy entity.
 */
@Data
@NoArgsConstructor
@Entity
public class Deputy {

  @Id
  @GeneratedValue
  private Long id;
  private String name;
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "deputy")
  @JsonIgnore
  private List<Vote> votes;

}
