package org.devchallenge.vote.processor.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Votes summary entity.
 */
@Data
@NoArgsConstructor
@Entity
public class VoteSummary {

  @Id
  @GeneratedValue
  private Long id;
  private Integer pro;
  private Integer contra;
  private Integer refrained;
  private Integer doesNotVote;
  private Integer absent;

}
