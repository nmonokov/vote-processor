package org.devchallenge.vote.processor.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * VoteSheet entity.
 */
@Data
@NoArgsConstructor
@Entity
public class VoteSheet {

  @Id
  @GeneratedValue
  private Long id;
  private String councilName;
  private String session;
  private @Lob String topic;
  private String subTopic;
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "voteSheet")
  private List<Vote> votes;
  @OneToOne(targetEntity = VoteSummary.class, cascade = CascadeType.ALL)
  private VoteSummary voteSummary;
  private Boolean accepted;

}
