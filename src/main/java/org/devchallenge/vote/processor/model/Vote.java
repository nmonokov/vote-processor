package org.devchallenge.vote.processor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Vote entity.
 */
@Data
@NoArgsConstructor
@Entity
public class Vote {

  @Id
  @GeneratedValue
  private Long id;
  private String choice;
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "deputy_id")
  private Deputy deputy;
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "voteSheet_id")
  @JsonIgnore
  private VoteSheet voteSheet;

}
