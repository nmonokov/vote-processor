package org.devchallenge.vote.processor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Deputy frequent choice model.
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude={"id", "deputyId", "choice"})
public class DeputyFrequentChoice implements Comparable<DeputyFrequentChoice> {

  @Id
  @GeneratedValue
  @JsonIgnore
  private Long id;
  private Long deputyId;
  private String choice;
  private Double frequency;

  @Override
  public int compareTo(DeputyFrequentChoice o) {
    return Double.compare(this.frequency, o.frequency);
  }
}
