package org.devchallenge.vote.processor.dao;

import lombok.NoArgsConstructor;

import org.devchallenge.vote.processor.model.Vote;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * Dao for votes.
 */
@Repository
@NoArgsConstructor
public class VoteDao extends GenericDao<Vote> {

  @Autowired
  private SessionFactory sessionFactory;

  @Override
  protected SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  @Override
  protected Class<Vote> getType() {
    return Vote.class;
  }
}
