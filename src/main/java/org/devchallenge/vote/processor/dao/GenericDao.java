package org.devchallenge.vote.processor.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Generic dao for all entities' transactions.
 */
@Transactional
public abstract class GenericDao<T> {

  @SuppressWarnings("unchecked")
  public void save(T entity) {
    Session session = getSessionFactory().getCurrentSession();
    session.save(entity);
  }

  public T get(Long id) {
    return getSessionFactory().getCurrentSession().get(getType(), id);
  }

  @SuppressWarnings("unchecked")
  public void merge(T entity) {
    Session session = getSessionFactory().getCurrentSession();
    session.merge(entity);
  }

  @SuppressWarnings("unchecked")
  public List<T> list() {
    return getSessionFactory().getCurrentSession().createCriteria(getType()).list();
  }

  protected abstract SessionFactory getSessionFactory();

  protected abstract Class<T> getType();

}
