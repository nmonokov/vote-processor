package org.devchallenge.vote.processor.dao;

import org.devchallenge.vote.processor.model.DeputyFrequentChoice;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Dao for deputy's frequent choices.
 */
@Repository
@Transactional
public class DeputyFrequentChoiceDao extends GenericDao<DeputyFrequentChoice> {

  @Autowired
  private SessionFactory sessionFactory;

  @Override
  protected SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  @Override
  protected Class<DeputyFrequentChoice> getType() {
    return DeputyFrequentChoice.class;
  }

  public DeputyFrequentChoice getByDeputyId(Long deputyId) {
    return (DeputyFrequentChoice) sessionFactory.getCurrentSession()
        .createCriteria(DeputyFrequentChoice.class)
        .add(Restrictions.eq("deputyId", deputyId))
        .uniqueResult();
  }
}
