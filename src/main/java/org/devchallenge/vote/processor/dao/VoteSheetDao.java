package org.devchallenge.vote.processor.dao;

import lombok.NoArgsConstructor;

import org.devchallenge.vote.processor.model.Deputy;
import org.devchallenge.vote.processor.model.VoteSheet;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * Dao for VoteSheet.
 */
@Repository
@NoArgsConstructor
public class VoteSheetDao extends GenericDao<VoteSheet> {

  @Autowired
  private SessionFactory sessionFactory;

  @Override
  protected SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  @Override
  protected Class<VoteSheet> getType() {
    return VoteSheet.class;
  }


}
