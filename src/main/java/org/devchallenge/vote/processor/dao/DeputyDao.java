package org.devchallenge.vote.processor.dao;

import lombok.NoArgsConstructor;

import org.devchallenge.vote.processor.model.Deputy;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Dao for deputies.
 */
@Repository
@NoArgsConstructor
public class DeputyDao extends GenericDao<Deputy> {

  @Autowired
  private SessionFactory sessionFactory;

  @Override
  protected SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  @Override
  protected Class<Deputy> getType() {
    return Deputy.class;
  }

  @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
  public Deputy getByName(String name) {
    return (Deputy) sessionFactory.getCurrentSession().createCriteria(Deputy.class)
        .add(Restrictions.eq("name", name))
        .uniqueResult();
  }

}
