package org.devchallenge.vote.processor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class VoteProcessorApplication {

  public static void main(String[] args) {
    SpringApplication.run(VoteProcessorApplication.class, args);
  }

}
