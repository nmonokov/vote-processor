package org.devchallenge.vote.processor.controller;

import org.devchallenge.vote.processor.model.Deputy;
import org.devchallenge.vote.processor.model.DeputyFrequentChoice;
import org.devchallenge.vote.processor.model.VoteSheet;
import org.devchallenge.vote.processor.service.DeputyFrequentChoiceService;
import org.devchallenge.vote.processor.service.VoteProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Application controller.
 */
@RestController
public class VoteProcessorController {

  @Autowired
  private VoteProcessorService service;

  @Autowired
  private DeputyFrequentChoiceService choiceService;

  /**
   * Get all vote sheets.
   */
  @GetMapping("/sheets")
  public List<VoteSheet> getVoteSheets() {
    return service.getVoteSheets();
  }

  /**
   * Get selected vote sheet.
   *
   * @param id id of the vote sheet
   */
  @GetMapping("/sheets/{id}")
  public VoteSheet get(@PathVariable("id") Long id) {
    return service.get(id);
  }

  /**
   *  Get all deputies.
   */
  @GetMapping("/deputies")
  public List<Deputy> getDeputies() {
    return service.getDeputies();
  }

  /**
   * Get frequent choices in percents of the deputies.
   */
  @GetMapping("/zone")
  public List<DeputyFrequentChoice> getDeputiesFrequentChoices() {
    return choiceService.getAllChoices();
  }

  /**
   * Get list of deputies which has those frequent choices as the selected deputy. Selection
   * can be expanded by query parameters
   *
   * @param id influence center deputy
   * @param lowerBorder expands lower border of the selection
   * @param upperBorder expands upper border of the selection
   */
  @GetMapping("/zone/{id}")
  public List<DeputyFrequentChoice> getInfluenceCenterByDeputy(@PathVariable("id") Long id,
      @RequestParam(value = "lowerBorder", required = false) Integer lowerBorder,
      @RequestParam(value = "upperBorder", required = false) Integer upperBorder) {
    return choiceService.getDeputiesCloserToInfluenceCenter(
        id, lowerBorder != null ? lowerBorder : 0, upperBorder != null ? upperBorder : 0);
  }

}
