package org.devchallenge.vote.processor.service;

import org.devchallenge.vote.processor.dao.DeputyDao;
import org.devchallenge.vote.processor.dao.DeputyFrequentChoiceDao;
import org.devchallenge.vote.processor.model.DeputyFrequentChoice;
import org.devchallenge.vote.processor.model.Vote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Service for determine frequent choice of deputies.
 */
@Service
public class DeputyFrequentChoiceService {

  @Autowired
  private DeputyFrequentChoiceDao choiceDao;

  @Autowired
  private DeputyDao deputyDao;

  /**
   * Retain all frequent choices of all deputies.
   *
   * @return all frequent choices
   */
  public List<DeputyFrequentChoice> getAllChoices() {
    return choiceDao.list();
  }

  /**
   * Calculates the most frequent choice of all deputies and saves it to DB.
   */
  public void calculateFrequentChoices() {

    deputyDao.list().stream()
        .forEach(deputy -> {
          DeputyFrequentChoice persistedDeputyChoice = choiceDao.getByDeputyId(deputy.getId());
          DeputyFrequentChoice deputyFrequentChoice = extractChoice(deputy.getVotes());
          if (deputyFrequentChoice != null) {
            deputyFrequentChoice.setDeputyId(deputy.getId());

            if (persistedDeputyChoice == null) {
              choiceDao.save(deputyFrequentChoice);
            } else {
              if (!persistedDeputyChoice.getChoice().equals(deputyFrequentChoice.getChoice())
                  || !persistedDeputyChoice.getFrequency()
                  .equals(deputyFrequentChoice.getFrequency())) {
                deputyFrequentChoice.setId(persistedDeputyChoice.getId());
                choiceDao.merge(deputyFrequentChoice);
              }
            }
          }
        });
  }

  private DeputyFrequentChoice extractChoice(List<Vote> votes) {
    List<String> filteredChoices = votes.stream()
        .filter(vote -> !"Відсутній".equals(vote.getChoice()))
        .filter(vote -> !"Не голосував".equals(vote.getChoice()))
        .map(Vote::getChoice)
        .collect(Collectors.toList());

    Map<String, Double> choicesMap = new HashMap<>();
    filteredChoices.stream()
        .forEach(choice -> choicesMap.merge(choice, 1.0, Double::sum));

    double choicesQuantity = filteredChoices.size();
    List<DeputyFrequentChoice> listOfDeputyFrequentChoices =
        choicesMap.entrySet().stream().map(entry -> {
          DeputyFrequentChoice deputyFrequentChoice = new DeputyFrequentChoice();
          deputyFrequentChoice.setChoice(entry.getKey());
          double frequency = (entry.getValue() / choicesQuantity) * 100;
          deputyFrequentChoice.setFrequency(Math.rint(100 * frequency) / 100);
          return deputyFrequentChoice;
        }).collect(Collectors.toList());

    if (listOfDeputyFrequentChoices.size() != 0) {
      return Collections.max(listOfDeputyFrequentChoices);
    } else {
      return null;
    }
  }

  /**
   * Retains frequent choices of deputies based on selected deputy choice and it's frequency.
   *
   * @param id selected deputy
   * @param lowerBorder expands lower border of the selection
   * @param upperBorder expands upper border of the selection
   * @return filtered group of deputies whose choices are similar to the choice of the selected
   * deputy.
   */
  public List<DeputyFrequentChoice> getDeputiesCloserToInfluenceCenter(Long id, Integer lowerBorder,
      Integer upperBorder) {

    DeputyFrequentChoice persistedChoice = choiceDao.getByDeputyId(id);
    List<DeputyFrequentChoice> allChoices = choiceDao.list();

    return allChoices.stream()
        .filter(choice -> choice.getChoice().equals(persistedChoice.getChoice()))
        .filter(choice -> persistedChoice.getFrequency() - lowerBorder <= choice.getFrequency())
        .filter(choice -> persistedChoice.getFrequency() + upperBorder >= choice.getFrequency())
        .collect(Collectors.toList());
  }
}
