package org.devchallenge.vote.processor.service;

import lombok.extern.java.Log;

import org.devchallenge.vote.processor.dao.DeputyDao;
import org.devchallenge.vote.processor.dao.VoteDao;
import org.devchallenge.vote.processor.dao.VoteSheetDao;
import org.devchallenge.vote.processor.model.Deputy;
import org.devchallenge.vote.processor.model.Vote;
import org.devchallenge.vote.processor.model.VoteSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for Vote Processor app.
 */
@Service
@Log
public class VoteProcessorService {

  @Autowired
  private VoteSheetDao voteSheetDao;

  @Autowired
  private DeputyDao deputyDao;

  @Autowired
  private VoteDao voteDao;

  public VoteSheet get(Long id) {
    return voteSheetDao.get(id);
  }

  public List<VoteSheet> getVoteSheets() {
    return voteSheetDao.list();
  }

  public List<Deputy> getDeputies() {
    return deputyDao.list();
  }

  /**
   * Saves VoteSheet, Deputy and it's Vote parsed from pdf.
   *
   * @param voteSheet parsed vote sheet
   * @param deputies parsed deputies
   */
  public void saveEntities(VoteSheet voteSheet, List<Deputy> deputies) {
    voteSheetDao.save(voteSheet);
    for (Deputy deputyToPersist : deputies) {
      Deputy deputy = deputyDao.getByName(deputyToPersist.getName());
      if (deputy != null) {
        Vote vote = deputyToPersist.getVotes().get(0);
        vote.setDeputy(deputy);
        vote.setVoteSheet(voteSheet);
        try {
          voteDao.save(vote);
        } catch (Exception e) {
          voteDao.merge(vote);
        }
      } else {
        Vote vote = deputyToPersist.getVotes().get(0);
        vote.setDeputy(deputyToPersist);
        vote.setVoteSheet(voteSheet);
        voteDao.save(vote);
      }
    }


  }


}
