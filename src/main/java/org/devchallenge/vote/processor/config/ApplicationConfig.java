package org.devchallenge.vote.processor.config;

import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Configuration class.
 */
@EnableTransactionManagement
@EnableScheduling
@Configuration
public class ApplicationConfig {

  @Bean
  public SessionFactory sessionFactory(HibernateEntityManagerFactory hemf){
    return hemf.getSessionFactory();
  }

}
