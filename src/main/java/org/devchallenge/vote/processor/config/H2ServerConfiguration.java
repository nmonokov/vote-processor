package org.devchallenge.vote.processor.config;


import org.h2.tools.Server;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.sql.SQLException;

/**
 * Configuration for H2 DB.
 */
@Configuration
@Profile("dev")
public class H2ServerConfiguration {

  // Web port, default 8082
  @Value("${h2.web.port:8082}")
  private String h2WebPort;

  /**
   * Web console for the embedded h2 database.
   *
   * Go to http://localhost:8082 and connect to the database "jdbc:h2:mem:testdb",
   * username "sa", password empty.
   */
  @Bean
  @ConditionalOnExpression("${h2.web.enabled:true}")
  public Server h2WebServer() throws SQLException {
    return Server.createWebServer("-web", "-webAllowOthers", "-webPort", h2WebPort).start();
  }
}
