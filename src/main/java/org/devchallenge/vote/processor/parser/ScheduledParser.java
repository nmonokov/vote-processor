package org.devchallenge.vote.processor.parser;

import static java.time.LocalTime.now;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

import lombok.SneakyThrows;
import lombok.extern.java.Log;

import org.devchallenge.vote.processor.model.Deputy;
import org.devchallenge.vote.processor.model.Vote;
import org.devchallenge.vote.processor.model.VoteSheet;
import org.devchallenge.vote.processor.model.VoteSummary;
import org.devchallenge.vote.processor.service.DeputyFrequentChoiceService;
import org.devchallenge.vote.processor.service.VoteProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Scheduled parser lookups selected directory and process pdf files.
 */
@Component
@Log
public class ScheduledParser {

  private static final int AVERAGE_PAGE_LENGTH = 1700;

  @Autowired
  private VoteProcessorService service;

  @Autowired
  private DeputyFrequentChoiceService choiceService;

  @Value("${vote-processor.default.input-folder}")
  private String processingDirPath;

  private File processingDir;

  private File archiveDir;

  /**
   * Lookup selected o default folder for pdf files every 30 seconds and processes them.
   */
  @Scheduled(fixedRate = 30000)
  private void lookupDirectory() {
    String path = processingDirPath;
    if (processingDirPath == null || processingDirPath.isEmpty()) {
      path = "." + File.separator + "votes";
    }

    processingDir = new File(path.replace("/", File.separator));

    File[] pdfs = processingDir.listFiles();
    if (pdfs != null && pdfs.length > 0) {

      archiveDir = new File(path + File.separator + "archive");
      if (!archiveDir.exists()) {
        archiveDir.mkdir();
      }

      Arrays.stream(pdfs)
          .filter(file -> file.getName().contains(".pdf"))
          .forEach(this::parsePdf);
    }
  }

  @SneakyThrows
  private void parsePdf(File inputFile) {
    PdfReader reader = new PdfReader(inputFile.getPath());
    log.info("Processing file :'" + inputFile.getPath() + "'");

    int numberOfPages = reader.getNumberOfPages();
    StringBuilder collectedPages = new StringBuilder(AVERAGE_PAGE_LENGTH * numberOfPages);
    for (int i = 1; i <= numberOfPages; i++) {
      collectedPages.append(PdfTextExtractor.getTextFromPage(reader, i)).append("\n");
    }

    String[] pages = collectedPages.toString()
        .split("Система поіменного голосування \"Рада Голос\"\n");
    if (pages.length == 1) {
      pages = collectedPages.toString()
          .split("Система поіменного голосування \"Рада Голос\"\n");
    }
    log.info("Pages to process: '" + pages.length + "'");

    LocalTime before = now();
    for (String page : pages) {
      if (page.isEmpty()) {
        continue;
      }
      VoteSheet voteSheet = extractVoteSheet(page);
      List<Deputy> deputies = extractDeputies(page);
      service.saveEntities(voteSheet, deputies);
    }

    choiceService.calculateFrequentChoices();

    archiveFile(inputFile, archiveDir);
    log.info("Processed in: '" + Duration.between(before, now()).getSeconds() + "' seconds.");
  }

  /**
   * Moving  processed file to selected folder.
   *
   * @param sourceFile processed file
   * @param targetDir selected folder
   */
  private void archiveFile(File sourceFile, File targetDir) {
    File targetFile;
    String fileName = sourceFile.getName();
    targetFile = new File(targetDir, fileName);
    if (sourceFile.renameTo(targetFile)) {
      log.info("Moving file '" + fileName + "' to '" + targetFile.getPath() + "'");
    }
  }

  private VoteSheet extractVoteSheet(String text) {

    String header = getSheetHeader(text);
    String[] headerRows = header.split("\n");
    String footer = getSheetFooter(text);
    String[] footerRows = footer.split("\n");

    VoteSheet voteSheet = new VoteSheet();
    extractHeader(voteSheet, headerRows);
    extractVoteSummary(voteSheet, footerRows);
    extractAssigned(voteSheet, footerRows[5]);

    return voteSheet;
  }

  private String getSheetHeader(String text) {
    return text.substring(0, text.indexOf("Прізвище, ім'я та"));
  }

  private String getSheetFooter(String text) {
    return text.substring(
        text.lastIndexOf("ПІДСУМКИ ГОЛОСУВАННЯ")+ "ПІДСУМКИ ГОЛОСУВАННЯ".length() + 1);
  }

  private void extractHeader(VoteSheet voteSheet, String[] headerRows) {
    voteSheet.setCouncilName(headerRows[0]);
    voteSheet.setSession(headerRows[1]);
    StringBuilder topicBuilder = new StringBuilder();
    int row = 3;
    for (; row < headerRows.length - 2; row++) {
      if (headerRows[row].isEmpty()) {
        continue;
      }
      if (headerRows[row].startsWith("-")) {
        topicBuilder.append(headerRows[row]).append("\n");
      } else {
        topicBuilder.append(headerRows[row]).append(" ");
      }
    }
    voteSheet.setTopic(topicBuilder.toString());
    voteSheet.setSubTopic(headerRows[row]);
  }

  private void extractVoteSummary(VoteSheet voteSheet, String[] footerRows) {
    VoteSummary voteSummary = new VoteSummary();
    Pattern pattern = Pattern.compile("\\W+(\\d+)");
    Matcher matcher = pattern.matcher(footerRows[0]);
    if (matcher.find()) {
      voteSummary.setPro(Integer.valueOf(matcher.group(1)));
    }
    matcher = pattern.matcher(footerRows[1]);
    if (matcher.find()) {
      voteSummary.setContra(Integer.valueOf(matcher.group(1)));
    }
    matcher = pattern.matcher(footerRows[2]);
    if (matcher.find()) {
      voteSummary.setRefrained(Integer.valueOf(matcher.group(1)));
    }
    matcher = pattern.matcher(footerRows[3]);
    if (matcher.find()) {
      voteSummary.setDoesNotVote(Integer.valueOf(matcher.group(1)));
    }
    matcher = pattern.matcher(footerRows[4]);
    if (matcher.find()) {
      voteSummary.setAbsent(Integer.valueOf(matcher.group(1)));
    }
    voteSheet.setVoteSummary(voteSummary);
  }

  private void extractAssigned(VoteSheet voteSheet, String footerRow) {
    Pattern pattern = Pattern.compile("\\W+:(\\W+)");
    Matcher matcher = pattern.matcher(footerRow);
    if (matcher.find()) {
      voteSheet.setAccepted(matcher.group(1).trim().equals("ПРИЙНЯТО"));
    }
  }

  private List<Deputy> extractDeputies(String text) {

    String regex = "(\\d+)\\s(([А-ЯЄІЇа-яєії]+\\s){1,3})(\\W+)";
    String additionalRegex = "(([А-ЯЄІЇа-яєії]+\\s){1,2})";
    Pattern pattern = Pattern.compile(regex);
    Pattern additionalPattern = Pattern.compile(additionalRegex);

    String[] rows = getTableFromPage(text).split("\n");
    List<Deputy> deputies = new ArrayList<>();
    ArrayDeque<Deputy> bufferedQue = new ArrayDeque<>(2);

    for (String row : rows) {

      Matcher matcher = pattern.matcher(row);
      if (row.matches("(" + regex + "){1,2}")) {

        while(matcher.find()) {
          Vote vote = new Vote();
          vote.setChoice(matcher.group(4).trim());

          Deputy deputy = new Deputy();
          String name = matcher.group(2).trim();
          String[] splittedName = name.split(" ");
          if (splittedName.length == 2) {
            deputy.setName(splittedName[0]);
          } else {
            deputy.setName(name);
          }
          deputy.setVotes(Arrays.asList(vote));
          deputies.add(deputy);
        }

        if (!bufferedQue.isEmpty()) {
          fixBrokenRecords(deputies, bufferedQue);
        }

      } else if (row.matches("(" + additionalRegex + "){1,2}")) {

        Matcher additionalMatcher = additionalPattern.matcher(row);
        while (additionalMatcher.find()) {
          Deputy deputy = new Deputy();
          deputy.setName(additionalMatcher.group(1));
          bufferedQue.add(deputy);
        }

      }
    }
    return deputies;
  }

  /**
   * If queue has 2 items, then it means that previous 2 deputies' names should be fixed.
   * If queue has 1 items, than detecting which deputy's name should be fixed.
   *
   * @param deputies all processed deputies
   * @param bufferedQue items which are patches for deputies names
   */
  private void fixBrokenRecords(List<Deputy> deputies, ArrayDeque<Deputy> bufferedQue) {
    if (bufferedQue.size() == 2) {
      fixDeputy(deputies, bufferedQue, deputies.size() - 2);
      fixDeputy(deputies, bufferedQue, deputies.size() - 1);
    } else {
      if (deputies.get(deputies.size() - 1).getName().split(" ").length == 3) {
        fixDeputy(deputies, bufferedQue, deputies.size() - 2);
      } else {
        fixDeputy(deputies, bufferedQue, deputies.size() - 1);
      }
    }
  }

  private void fixDeputy(List<Deputy> deputies, ArrayDeque<Deputy> bufferedQue, Integer position) {
    Deputy deputyToFix = deputies.get(position);
    Deputy deputyWithName = bufferedQue.pop();
    deputyToFix.setName(deputyWithName.getName() + deputyToFix.getName());
  }

  private String getTableFromPage(String text)  {
    return text.substring(
        text.lastIndexOf("по-батькові депутата") + "по-батькові депутата".length() + 1,
        text.lastIndexOf("ПІДСУМКИ ГОЛОСУВАННЯ"));
  }
}
